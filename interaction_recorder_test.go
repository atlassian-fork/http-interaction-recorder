package recorder_test

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/mocks"
	"github.com/stretchr/testify/assert"
)

func whenDefaultRequestIsProcessed(interceptor http.Handler) {
	request, _ := http.NewRequest("GET", "", nil)
	whenRequestIsProcessed(interceptor, request)
}

func whenRequestIsProcessed(interceptor http.Handler, request *http.Request) {
	interceptor.ServeHTTP(httptest.NewRecorder(), request)
}

func createSimpleHttpHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
}

func Test_Listeners(t *testing.T) {
	t.Run("it should call all listeners after interaction is finished", func(t *testing.T) {
		listenerOne := mocks.NewMockListener()
		listenerTwo := mocks.NewMockListener()
		interceptor := recorder.Handler(createSimpleHttpHandler(), listenerOne, listenerTwo)

		whenDefaultRequestIsProcessed(interceptor)

		assert.Equal(t, 1, listenerOne.CalledTimes(), "Expected listener one to be called 1 times, but was called %v", listenerOne.CalledTimes())
		assert.Equal(t, 1, listenerTwo.CalledTimes(), "Expected listener two to be called 1 times, but was called %v", listenerOne.CalledTimes())
	})
}

// nolint[: gocyclo]
func Test_Request(t *testing.T) {
	t.Run("it should provide request method in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, "GET", listenerRequest.Method, "Expected requests method to be '%v', but instead received '%v'", "GET", listenerRequest.Method)
	})

	t.Run("it should provide request path in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "http://localhost:8080/some/url", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, "/some/url", listenerRequest.Path, "Expected requests path to be '%v', but instead received '%v'", "/some/url", listenerRequest.Path)
	})

	t.Run("it should not escape request path", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/%v", "some%2Fweird%2Fpath"), nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		expectedPath := "/some%2Fweird%2Fpath"
		assert.Equal(t, expectedPath, listenerRequest.Path, "Expected requests path to be '%v', but instead received '%v'", expectedPath, listenerRequest.Path)
	})

	t.Run("it should provide request path fragment in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "http://localhost:8080/some/url#fragment", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, "fragment", listenerRequest.Fragment, "Expected requests fragment to be '%v', but instead received '%v'", "#fragment", listenerRequest.Fragment)
	})

	t.Run("it should not include fragment in the path", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "http://localhost:8080/some/url#fragment", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, "/some/url", listenerRequest.Path, "Expected requests path to be '%v', but instead received '%v'", "/some/url", listenerRequest.Path)
	})

	t.Run("it should provide request query in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		expectedQuery := "query=value&key=anotherValue"
		request, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/some/url?%v", expectedQuery), nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, expectedQuery, listenerRequest.Query, "Expected requests query to be '%v', but instead received '%v'", expectedQuery, listenerRequest.Query)
	})

	t.Run("it should not include query in the path", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "http://localhost:8080/some/url?query=value&key=anotherValue", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(t, "/some/url", listenerRequest.Path, "Expected requests path to be '%v', but instead received '%v'", "/some/url", listenerRequest.Path)
	})

	t.Run("it should provide request body in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		request, err := http.NewRequest("GET", "", bytes.NewBuffer([]byte(`{"json": "payload"}`)))
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		expectedPayload := `{"json": "payload"}`
		actualBody, _ := ioutil.ReadAll(listenerRequest.Body)
		assert.Equal(t, expectedPayload, string(actualBody), "Expected request body to be '%v', but instead received '%v'", expectedPayload, string(actualBody))
	})

	t.Run("it should copy request body before calling the original handler", func(t *testing.T) {
		var bodyFromHandler string
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				bodyFromHandler = fmt.Sprintf("Could not read requests body: %v", err.Error())
			}
			bodyFromHandler = string(body)
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		request, err := http.NewRequest("POST", "", bytes.NewBuffer([]byte(`{"json": "payload"}`)))
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		actualBody, err := ioutil.ReadAll(listenerRequest.Body)
		assert.NoError(t, err)
		assert.Equal(t, bodyFromHandler, string(actualBody), "Expected request body to be equal, but instead received listener: '%v', handler: '%v'", string(actualBody), bodyFromHandler)
	})

	t.Run("it should set body to nil, if the original body was nil", func(t *testing.T) {
		var requestBody io.Reader
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestBody = r.Body
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		request, err := http.NewRequest("GET", "", nil)
		assert.NoError(t, err)
		whenRequestIsProcessed(recorderHandler, request)

		assert.Equal(t, nil, requestBody, "Expected request body to be nil, but received %T", requestBody)
	})

	t.Run("it should provide request headers in the listener", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		headerName := "TestRequestHeader"
		expectedHeaderValue := "value"
		request, err := http.NewRequest("GET", "", nil)
		assert.NoError(t, err)
		request.Header.Add(headerName, expectedHeaderValue)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		assert.Equal(
			t,
			expectedHeaderValue,
			listenerRequest.Header.Get(headerName),
			"Expected header %v to have value '%v', but instead received '%v'", headerName, expectedHeaderValue, listenerRequest.Header.Get(headerName),
		)
	})

	t.Run("it should copy original request headers", func(t *testing.T) {
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(createSimpleHttpHandler(), listener)

		headerName := "TestRequestHeader"
		expectedHeaderValue := "value"
		request, err := http.NewRequest("GET", "", nil)
		assert.NoError(t, err)
		request.Header.Add(headerName, expectedHeaderValue)
		whenRequestIsProcessed(recorderHandler, request)

		listenerRequest := listener.GetRequest()
		listenerRequest.Header.Del(headerName)
		assert.Equal(t, expectedHeaderValue, request.Header.Get(headerName), "Expected original header %v to have value '%v', but instead received '%v'", headerName, expectedHeaderValue, listenerRequest.Header.Get(headerName))
	})
}

func Test_Response(t *testing.T) {
	t.Run("it should provide response status in the listener", func(t *testing.T) {
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusUnauthorized)
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		whenDefaultRequestIsProcessed(recorderHandler)

		assert.Equal(t, http.StatusUnauthorized, listener.GetResponse().StatusCode, "Expected response status to be %v, but received %v", http.StatusUnauthorized, listener.GetResponse().StatusCode)
	})

	t.Run("it should provide response headers in the listener", func(t *testing.T) {
		headerName := "TestResponseHeader"
		expectedHeaderValue := "value"
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add(headerName, expectedHeaderValue)
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		whenDefaultRequestIsProcessed(recorderHandler)

		listenerResponse := listener.GetResponse()
		actualHeaderValue := listenerResponse.Header.Get(headerName)
		assert.Equal(t, expectedHeaderValue, actualHeaderValue, "Expected response header '%v' to be '%v', but received '%v'", headerName, expectedHeaderValue, actualHeaderValue)
	})

	t.Run("it should copy original response headers", func(t *testing.T) {
		headerName := "TestResponseHeader"
		expectedHeaderValue := "value"
		var originalResponseHeader http.Header
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add(headerName, expectedHeaderValue)
			originalResponseHeader = w.Header()
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		whenDefaultRequestIsProcessed(recorderHandler)

		listenerResponse := listener.GetResponse()
		listenerResponse.Header.Del(headerName)
		assert.Equal(
			t,
			expectedHeaderValue,
			originalResponseHeader.Get(headerName),
			"Expected original response header '%v' to be '%v', but received '%v'", headerName, expectedHeaderValue, originalResponseHeader.Get(headerName),
		)
	})

	t.Run("it should provide response body in the listener", func(t *testing.T) {
		expectedPayload := `{"json": "payload"}`
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(expectedPayload))
		})
		listener := mocks.NewMockListener()
		recorderHandler := recorder.Handler(handler, listener)

		whenDefaultRequestIsProcessed(recorderHandler)

		listenerResponse := listener.GetResponse()
		actualBody, err := ioutil.ReadAll(listenerResponse.Body)
		assert.NoError(t, err)
		assert.Equal(t, expectedPayload, string(actualBody), "Expected response body '%v', but received '%v'", expectedPayload, string(actualBody))
	})
}
