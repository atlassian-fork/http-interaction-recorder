package pact_test

import (
	"bytes"
	"fmt"
	"testing"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestJsonBodySerialization(t *testing.T) {
	t.Run("it should serialize body as json object, if it is an object", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte("{}")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte("{}")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			body := test.getBodyFunc(pact)
			_, isAJsonObject := body.(map[string]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a json object", test.name))
		}
	})

	t.Run("it should serialize body as array, if it is an array", func(t *testing.T) {
		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte("[]")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte("[]")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			body := test.getBodyFunc(pact)
			_, isAJsonObject := body.([]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be an array", test.name))
		}
	})

	t.Run("it should serialize body as string, if it is a string", func(t *testing.T) {
		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte(`"string"`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte(`"string"`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			body := test.getBodyFunc(pact)
			_, isAJsonObject := body.(string)
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a string", test.name))
		}
	})

	t.Run("it should serialize body as number, if it is a number", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte("1")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte("1")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			body := test.getBodyFunc(pact)
			_, isAJsonObject := body.(float64)
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a number", test.name))
		}
	})

	t.Run("it should serialize body as boolean, if it is a boolean", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte("true")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte("false")),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			body := test.getBodyFunc(pact)
			_, isAJsonObject := body.(bool)
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a boolean", test.name))
		}
	})

	t.Run("it should serialize body with a null type", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte(`{"value": null}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte(`{"value": null}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			rawBody := test.getBodyFunc(pact)
			body, isAJsonObject := rawBody.(map[string]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a json object", test.name))
			assert.Equal(t, body["value"], nil)
		}
	})

	t.Run("it should serialize body with a number type", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte(`{"value": 12}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte(`{"value": 12}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			rawBody := test.getBodyFunc(pact)
			body, isAJsonObject := rawBody.(map[string]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a json object", test.name))
			assert.Equal(t, body["value"], float64(12))
		}
	})

	t.Run("it should serialize body with a bool type", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte(`{"value": true}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte(`{"value": true}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			rawBody := test.getBodyFunc(pact)
			body, isAJsonObject := rawBody.(map[string]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a json object", test.name))
			assert.Equal(t, body["value"], true)
		}
	})

	t.Run("it should serialize body with a string type", func(t *testing.T) {

		tests := []struct {
			name        string
			option      generatorOption
			getBodyFunc func(pact jsonPact) interface{}
		}{
			{
				name: "request",
				option: request(recorder.Request{
					Body: bytes.NewBuffer([]byte(`{"value": "string"}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstRequestRawBody()
				},
			},
			{
				name: "response",
				option: response(recorder.Response{
					Body: bytes.NewBuffer([]byte(`{"value": "string"}`)),
				}),
				getBodyFunc: func(pact jsonPact) interface{} {
					return pact.getFirstResponseRawBody()
				},
			},
		}

		for _, test := range tests {
			invocation := newPactFileGeneratorInvocation(test.option)

			err := invocation.invokeProcess()

			require.NoError(t, err)
			pact := invocation.getPactAsJson()
			rawBody := test.getBodyFunc(pact)
			body, isAJsonObject := rawBody.(map[string]interface{})
			assert.True(t, isAJsonObject, fmt.Sprintf("%v body should be a json object", test.name))
			assert.Equal(t, body["value"], "string")
		}
	})
}
