package pact_test

import (
	"bytes"
	"net/http"
	"testing"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPactContent(t *testing.T) {
	t.Run("it should include consumers name", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(consumer("consumer"))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "consumer", pact.getConsumer(), "consumer name")
	})

	t.Run("it should include providers name", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(provider("provider"))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "provider", pact.getProvider(), "provider name")
	})

	t.Run("it should panic if consumers name is empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(consumer(" "))

		invoke := func() { invocation.invokeProcess() }

		assert.Panics(t, invoke, "should panic if consumer name is empty")
	})

	t.Run("it should panic if provider name is empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(provider(" "))

		invoke := func() { invocation.invokeProcess() }

		assert.Panics(t, invoke, "should panic if provider name is empty")
	})

	t.Run("it should capture requests method", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Method: "GET",
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "GET", pact.getFirstRequestMethod())
	})

	t.Run("it should capture requests path", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Path: "/path",
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "/path", pact.getFirstRequestPath())
	})

	t.Run("it should not capture requests fragment", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Path:     "/path",
			Fragment: "fragment",
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "/path", pact.getFirstRequestPath())
	})

	t.Run("it should capture requests query string", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Query: "query=foo&bar=bar1",
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "query=foo&bar=bar1", pact.getFirstRequestQuery())
	})

	t.Run("it should omit requests query if empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Query: "",
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		firstInteractionRequest := pact.getFirstInteractionRequest()
		_, isPresent := firstInteractionRequest["query"]
		assert.False(t, isPresent)
	})

	t.Run("it should capture request headers", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Header: http.Header{"accept": []string{"application/json"}, "custom": []string{"header"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstRequestHeaders()
		assert.Equal(t, map[string]interface{}{"accept": "application/json", "custom": "header"}, actualHeaders)
	})

	t.Run("it should normalize request header names", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Header: http.Header{"We-IrD-HeaDer": []string{"header"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstRequestHeaders()
		assert.Equal(t, map[string]interface{}{"we-ird-header": "header"}, actualHeaders)
	})

	t.Run("it should concatenate multiple request header values", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Header: http.Header{"accept": []string{"application/json", "application/xml"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstRequestHeaders()
		assert.Equal(t, map[string]interface{}{"accept": "application/json, application/xml"}, actualHeaders)
	})

	t.Run("it should omit request headers if empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Header: http.Header{},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		firstInteractionRequest := pact.getFirstInteractionRequest()
		_, isPresent := firstInteractionRequest["headers"]
		assert.False(t, isPresent)
	})

	t.Run("it should capture requests body", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Body: bytes.NewBuffer([]byte(`"some request body"`)),
		}))

		err := invocation.invokeProcess()

		require.NoError(t, err)
		pact := invocation.getPactAsJson()
		assert.Equal(t, "some request body", pact.getFirstRequestBody())
	})

	t.Run("it should omit request body if empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(request(recorder.Request{
			Body: nil,
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		firstInteractionRequest := pact.getFirstInteractionRequest()
		_, isPresent := firstInteractionRequest["body"]
		assert.False(t, isPresent)
	})

	t.Run("it should capture response status code", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			StatusCode: 201,
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, 201, pact.getFirstResponseStatusCode())
	})

	t.Run("it should capture response headers", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Header: http.Header{"accept": []string{"application/json"}, "custom": []string{"header"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstResponseHeaders()
		assert.Equal(t, map[string]interface{}{"accept": "application/json", "custom": "header"}, actualHeaders)
	})

	t.Run("it should normalize response header names", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Header: http.Header{"We-IrD-HeaDer": []string{"header"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstResponseHeaders()
		assert.Equal(t, map[string]interface{}{"we-ird-header": "header"}, actualHeaders)
	})

	t.Run("it should concatenate multiple response header values", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Header: http.Header{"accept": []string{"application/json", "application/xml"}},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		actualHeaders := pact.getFirstResponseHeaders()
		assert.Equal(t, map[string]interface{}{"accept": "application/json, application/xml"}, actualHeaders)
	})

	t.Run("it should omit response headers if empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Header: http.Header{},
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		firstInteractionResponse := pact.getFirstInteractionResponse()
		_, isPresent := firstInteractionResponse["headers"]
		assert.False(t, isPresent)
	})

	t.Run("it should capture response body", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Body: bytes.NewBuffer([]byte(`"response body"`)),
		}))

		err := invocation.invokeProcess()

		require.NoError(t, err)
		pact := invocation.getPactAsJson()
		assert.Equal(t, "response body", pact.getFirstResponseBody())
	})

	t.Run("it should omit response body if empty", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(response(recorder.Response{
			Body: nil,
		}))

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		firstInteractionResponse := pact.getFirstInteractionResponse()
		_, isPresent := firstInteractionResponse["body"]
		assert.False(t, isPresent)
	})

	t.Run("it should include the interaction description", func(t *testing.T) {
		invocation := newPactFileGeneratorInvocation(
			request(recorder.Request{
				Method: "GET",
				Path:   "/some/path",
			}),
			response(recorder.Response{StatusCode: 200}),
		)

		invocation.invokeProcess()

		pact := invocation.getPactAsJson()
		assert.Equal(t, "GET /some/path -> 200", pact.getFirstInteractionDescription())
	})
}
