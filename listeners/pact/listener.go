package pact

import (
	"bitbucket.org/atlassian/http-interaction-recorder"
)

type Listener struct {
	consumer string
	provider string
}

func (listener Listener) RequestReceived(request recorder.Request, response recorder.Response) {
	registry := registry()
	err := registry.processInteraction(
		listener.consumer,
		listener.provider,
		interaction{
			request:  request,
			response: response,
		},
	)
	if err != nil {
		panic(err)
	}
}

func (listener Listener) GetPactLocation() string {
	registry := registry()
	return registry.getPactLocation(listener.consumer, listener.provider)
}

func NewListener(consumer, provider string) recorder.Listener {
	return Listener{
		consumer: consumer,
		provider: provider,
	}
}
