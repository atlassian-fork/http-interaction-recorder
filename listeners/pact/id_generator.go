package pact

import (
	"crypto/rand"
	"fmt"
	"io"
)

type IDGenerator interface {
	Generate() string
}

type idGenerator struct{}

func (generator idGenerator) Generate() string {
	uuid := make([]byte, 16)
	_, err := io.ReadFull(rand.Reader, uuid)
	if err != nil {
		panic(err)
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

func newIDGenerator() IDGenerator {
	return idGenerator{}
}
