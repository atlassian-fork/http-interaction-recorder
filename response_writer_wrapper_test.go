package recorder_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/mocks"
	"github.com/stretchr/testify/assert"
)

func whenInteractionIsProcessed(originalHandler http.Handler, responseRecorder http.ResponseWriter) {
	handler := recorder.Handler(originalHandler)

	request, _ := http.NewRequest("GET", "", nil)
	handler.ServeHTTP(responseRecorder, request)
}

func createNewMockResponseWriter() *mocks.MockResponseWriter {
	basicWriter := mocks.MockBasicResponseWriter{}
	return &mocks.MockResponseWriter{
		basicWriter,
		mocks.MockHijackerResponseWriter{&basicWriter, false},
		mocks.MockFlusherResponseWriter{&basicWriter, false},
		mocks.MockCloseNotifierResponseWriter{&basicWriter, false},
	}
}

func Test_ResponseWrapper(t *testing.T) {
	t.Run("it should call WriteHeader on the original response", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNoContent)
		})

		responseRecorder := httptest.NewRecorder()
		whenInteractionIsProcessed(originalHandler, responseRecorder)

		assert.Equal(t, http.StatusNoContent, responseRecorder.Code, "Expected WriteHeader to have been called with %v, but was called with %v", http.StatusOK, responseRecorder.Code)
	})

	t.Run("it should call Write on the original response", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(`Test Response Body`))
		})

		responseRecorder := httptest.NewRecorder()
		whenInteractionIsProcessed(originalHandler, responseRecorder)

		assert.Equal(t, "Test Response Body", responseRecorder.Body.String(), "Expected Write to have been called with %v, but was called with %v", "Test", responseRecorder.Body)
	})

	t.Run("it should call CloseNotify on the original response", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.(http.CloseNotifier).CloseNotify()
		})

		mockResponseWriter := createNewMockResponseWriter()
		whenInteractionIsProcessed(originalHandler, mockResponseWriter)

		assert.True(t, mockResponseWriter.CalledCloseNotify, "Expected CloseNotify to have been called")
	})

	t.Run("it should call Flush on the original response", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.(http.Flusher).Flush()
		})

		mockResponseWriter := createNewMockResponseWriter()
		whenInteractionIsProcessed(originalHandler, mockResponseWriter)

		assert.True(t, mockResponseWriter.CalledFlush, "Expected Flush to have been called")
	})

	t.Run("it should call Hijack on the original response", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.(http.Hijacker).Hijack()
		})

		mockResponseWriter := createNewMockResponseWriter()
		whenInteractionIsProcessed(originalHandler, mockResponseWriter)

		assert.True(t, mockResponseWriter.CalledHijack, "Expected Hijack to have been called")
	})

	t.Run("it should not override status code after calling Write", func(t *testing.T) {
		originalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(`Test`))
		})

		responseRecorder := createNewMockResponseWriter()
		whenInteractionIsProcessed(originalHandler, responseRecorder)

		assert.Equal(t, http.StatusCreated, responseRecorder.Code, "Expected status code to be %v, but it was %v", http.StatusCreated, responseRecorder.Code)
	})
}
