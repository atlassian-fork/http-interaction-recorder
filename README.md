# HTTP Interaction Recorder
> Captures HTTP request/responses interactions in any server, which supports `http.Handler` and standard middleware. One of the use cases is to generate Pact files
> to verify, via contract testing ([swagger-mock-validator](https://bitbucket.org/atlassian/swagger-mock-validator)), that mocks are consistent with real provider.

## Requirements
- go 1.9 or higher

## Installation

Get the `httptest-interaction-listener` as a dependency

```
go get bitbucket.org/atlassian/httptest-interaction-listener
```

## Usage

HTTP Interaction Recorder provides a stdlib net/http middleware handler to record http interactions, which makes it compatible with any standard middleware in the community.
Recording or any postprocessing of an interaction takes place in listeners, which are supplied to a middleware and called after response was sent to client.
Listeners must implement `RequestReceived(request recorder.Request, response recorder.Response)` method.

```golang
listener := createCustomListener()
middleware := recorder.Middleware(listener)
```

If you are not using a middleware and just want to wrap an http.Handler to record its interactions, you can use `Handler` method

```golang
listener := createCustomListener()
handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte(expectedPayload))
})

recordingHandler := recorder.Handler(handler, listener)
```

### Generating pact files for recorded interactions

`pact.Listener` implements a listener to record interactions and save them as [pact](https://docs.pact.io/) file. This file can then be used to verify the mocked responses used in tests.

```
import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/listeners/pact"
)

listener := pact.NewListener("consumer", "provider")
mockProviderHandler := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
    writer.WriteHeader(201)
    writer.Header().Add("custom-response-header", "response-header-value")
    writer.Write([]byte(`"mocked out response body"`))
})
mockServer := httptest.NewServer(recorder.Handler(mockProviderHandler, listener))

providerClient.CallProvider()
```

In this example a "consumer" service mocks out response from a "provider" service using a stdlib `httptest.NewServer`. Pact listener will
record every interaction with `httptest.NewServer` and save it in the `./pacts` folder next to the test file.

## Changelog
See [CHANGELOG.md](CHANGELOG.md)

## Contributing
See [CONTRIBUTING.md](CONTRIBUTING.md)

## License
See [LICENSE.txt](LICENSE.txt)